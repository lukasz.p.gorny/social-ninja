package pro.lukasgorny.socialninja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialninjaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocialninjaApplication.class, args);
    }

}

